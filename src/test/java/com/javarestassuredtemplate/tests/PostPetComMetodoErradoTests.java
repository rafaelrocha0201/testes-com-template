package com.javarestassuredtemplate.tests;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.requests.PostPetMetodoErradoRequest;
import com.javarestassuredtemplate.requests.PostPetRequest;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;

import static org.hamcrest.Matchers.equalTo;

public class PostPetComMetodoErradoTests extends TestBase {

    PostPetMetodoErradoRequest postPetMetodoErradoRequest;

    @Test
    public void inserirPetComMetodoErrado(){
        //Parâmetros
        String id = "9999";
        int categoryId = 9999;
        String categoryName = "felinos";
        String name = "Shepherd";
        String photoUrl = "http://photodogatito.com/image123.png";
        int tagId = 9999;
        String tagName = "macho";
        String status = "available";
        int statusCodeEsperado = HttpStatus.SC_METHOD_NOT_ALLOWED;

        //Fluxo
        postPetMetodoErradoRequest = new PostPetMetodoErradoRequest();
        postPetMetodoErradoRequest.setJsonBodyUsingJsonFile(id, categoryId, categoryName, name, photoUrl, tagId, tagName, status);
        ValidatableResponse response = postPetMetodoErradoRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
    }
}
