package com.javarestassuredtemplate.tests;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.requests.GetPetByStatusRequest;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;

public class GetPetByStatusTests extends TestBase {

    GetPetByStatusRequest getPetByStatusRequest;

    @Test
    public void buscarPetPorStatus(){
        //Parâmetros
        String status = "pending";
        int statusCodeEsperado = HttpStatus.SC_OK;
        int quant = 14;

        //Fluxo
        getPetByStatusRequest = new GetPetByStatusRequest(status);
        ValidatableResponse response = getPetByStatusRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.body(hasSize(quant));
    }
}
