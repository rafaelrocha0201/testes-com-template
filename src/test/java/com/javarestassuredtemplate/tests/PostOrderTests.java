package com.javarestassuredtemplate.tests;

import com.javarestassuredtemplate.bases.TestBase;
import com.javarestassuredtemplate.requests.PostOrderRequest;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.equalTo;

public class PostOrderTests extends TestBase {

    PostOrderRequest postOrderRequest;

    @Test
    public void cadastrarPedidoDePet()
    {
        //Parâmetros
        int id = 1;
        int petId = 1;
        int quantity = 1;
        String shipDate = "2022-07-06T15:21:57.159+0000";
        String status = "available";
        boolean complete = true;
        int statusCodeEsperado = HttpStatus.SC_OK;

        //Fluxo
        postOrderRequest = new PostOrderRequest();
        postOrderRequest.setJsonBodyUsingJsonFile(id, petId, quantity, shipDate, status, complete);
        ValidatableResponse response = postOrderRequest.executeRequest();

        //Asserções
        response.statusCode(statusCodeEsperado);
        response.body("id", equalTo(id),
                "petId", equalTo(petId),
                "quantity", equalTo(quantity),
                "shipDate", equalTo(shipDate),
                "status", equalTo(status),
                "complete", equalTo(true));
    }
}
