package com.javarestassuredtemplate.jsonObjects;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Order {
    private float id;
    private float petId;
    private float quantity;
    private String shipDate;
    private String status;
    private boolean complete;
}
