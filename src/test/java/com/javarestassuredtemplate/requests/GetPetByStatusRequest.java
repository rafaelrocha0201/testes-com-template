package com.javarestassuredtemplate.requests;

import com.javarestassuredtemplate.bases.RequestRestBase;
import io.restassured.http.Method;

public class GetPetByStatusRequest extends RequestRestBase {

    public GetPetByStatusRequest(String status){
        requestService = "/pet/findByStatus";
        method = Method.GET;
        queryParameters.put("status", status);
    }
}
