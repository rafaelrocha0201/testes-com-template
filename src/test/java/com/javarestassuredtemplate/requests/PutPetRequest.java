package com.javarestassuredtemplate.requests;

import com.javarestassuredtemplate.bases.RequestRestBase;
import com.javarestassuredtemplate.jsonObjects.Category;
import com.javarestassuredtemplate.jsonObjects.Pet;
import com.javarestassuredtemplate.jsonObjects.Tag;
import io.restassured.http.Method;

public class PutPetRequest extends RequestRestBase {

    public PutPetRequest(){
        requestService = "/pet";
        method = Method.PUT;
    }

    public void setJsonBodyUsingJavaObject(String id,
                                           int categoryId,
                                           String categoryName,
                                           String name,
                                           String photoUrl,
                                           int tagId,
                                           String tagName,
                                           String status){
        jsonBody = new Pet(id,
                new Category(categoryId, categoryName),
                name,
                new String[]{photoUrl},
                new Tag[]{new Tag(tagId, tagName)},
                status);
    }

    public void setJsonBodyUsingJavaObject(Object jsonObject){
        jsonBody = jsonObject;
    }
}
