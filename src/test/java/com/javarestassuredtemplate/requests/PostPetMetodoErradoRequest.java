package com.javarestassuredtemplate.requests;

import com.javarestassuredtemplate.bases.RequestRestBase;
import com.javarestassuredtemplate.utils.GeneralUtils;
import io.restassured.http.Method;

public class PostPetMetodoErradoRequest extends RequestRestBase {

    public PostPetMetodoErradoRequest(){
        requestService = "/pet";
        method = Method.GET;
    }

    public void setJsonBodyUsingJsonFile(String id,
                                         int categoryId,
                                         String categoryName,
                                         String name,
                                         String photoUrl,
                                         int tagId,
                                         String tagName,
                                         String status){
        jsonBody = GeneralUtils.readFileToAString("src/test/java/com/javarestassuredtemplate/jsons/PostPetJson.json")
                .replace("$id", String.valueOf(id))
                .replace("$categoryId", String.valueOf(categoryId))
                .replace("$categoryName", categoryName)
                .replace("$name", name)
                .replace("$photoUrl", photoUrl)
                .replace("$tagId", String.valueOf(tagId))
                .replace("$tagName", tagName)
                .replace("$status", status);
    }
}
